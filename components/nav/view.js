import html from '/js/html.js'
const template = html`
		<nav>
				<div class="nav-wrapper">
						<a href="#" class="brand-logo">test</a>
						<ul id="nav-mobile" class="right hide-on-med-and-down">

								<li v-for="route in routes" v-if="route.name" :class="{active:isActive(route)}" >
										 <router-link :to="route.path" >{{route.name}} </router-link>
								</li>
						</ul>
				</div>
		</nav>	

`


export default {
		props:["routes"],
		methods:{
			isActive(route){
					return route.path == this.$router.currentRoute.path
			}
		},
		template
}

