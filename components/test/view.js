import html from '/js/html.js'
import qraphql from '/js/qraphql.js'

const template = html`
		<div>
				<div class="row">
								<div class="col s12 m6">
										<div class="card red darken-3">
												<div class="card-content white-text">
														<input class="card-title" v-model="name">
												</div>
												<div class="card-action">
														<a href="#" @click="remove(id)" >remove</a>
														<a href="#" @click="save(id)" >save</a>
												</div>
										</div>
								</div>
				</div>
		</div>	

`


export default {
		data(){
				return {
						id:this.$route.params.id,
						name:''
				}
		},
		async created(){
				this.load()	
		},
		methods:{
				async remove(id){
						const q = `
						mutation{
								removeTest(ID: "${this.id}")
						}
						`
						const data = await qraphql(q)
						this.$router.push('/test');

				},
				async save(){
						const q = `
						mutation{
								updateTest(ID: "${this.id}" name: "${this.name}")
						}
						
						`
						const data = await qraphql(q)
						this.$router.push('/Test');
				},
				async load(){
						const data = await qraphql(`{tests(_id:"${this.id}"){id,name}}`)
						this.name = data.data.tests[0].name
						this.$forceUpdate();
				},

		},
		template
}

