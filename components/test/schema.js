import Model from "./model.js"
import { makeExecutableSchema } from '@graphql-tools/schema';


export default  makeExecutableSchema({
		typeDefs: `
		type Query {
				tests(name:String,_id:String): [Test!]!
		}
		type Test {
				id: ID!
						name: String!
		}
		type Mutation {
				createTest(name: String!): Test!
				removeTest(ID: ID): Boolean! 
				updateTest(ID: ID name:String): Boolean!

		}
		`,
		resolvers:{
				Query:{
						tests: (_,args) => Model.find(args)
				},
				Mutation:{
						createTest:async (_, { name }) => new Model({ name }).save(),
						removeTest:async (_, { ID}) => {
								await Model.remove({ _id:ID})
								return true 
						},
						updateTest:async (_, { ID,name}) => {
							  await Model.updateOne({ _id:ID,name})
								return true 
						}

				}
		}
})

