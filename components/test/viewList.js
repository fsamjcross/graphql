import html from '/js/html.js'
import qraphql from '/js/qraphql.js'

const template = html`
		<div>
				<div v-for="test in tests">
						<div class="row">
								<div class="col s12 m6">
										<div class="card red darken-3">
												<div class="card-content white-text">
														<span class="card-title">{{test.name}}</span>
												</div>
												<div class="card-action">
														<a href="#" @click="remove(test.id)" >remove</a>
														<router-link :to="'test/'+test.id">edit</router-link>
												</div>
										</div>
								</div>
						</div>	
				</div>
				<div>
						<div class="row">
								<div class="input-field col s6">
										<input id="NameInput" v-model="newTest.name" type="text" class="validate">
										<label class="active" for="NameInput">Name</label>
								</div>
								<div class="input-field col s6">
										<a @click="save" class="waves-effect waves-light btn">Add</a>
								</div>

						</div>
				</div>
		</div>	

`

export default {
		data(){
				return {
						tests:[],
						newTest:{
							name:''		
						}
				}
		},
		async created(){
				this.load()
		},
		methods:{
				async load(){
						const data = await qraphql(`{tests{id,name}}`)
						this.tests = data.data.tests
						this.$forceUpdate();
				},
				async save(){
						const q = `
						mutation{
								createTest(name: "${this.newTest.name}"){
										id
										name
								}
						}							
						`
						const data = await qraphql(q)
						this.tests.push(data.data.createTest)
				},
				async remove(id){
						const q = `
						mutation{
								removeTest(ID: "${id}")
						}
						`
						const data = await qraphql(q)
						this.load()

				}
		},
		template
}

