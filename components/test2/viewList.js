import html from '/js/html.js'
import qraphql from '/js/qraphql.js'

const template = html`
		<div>
				<div v-for="test2 in test2s">
						<div class="row">
								<div class="col s12 m6">
										<div class="card red darken-3">
												<div class="card-content white-text">
														<span class="card-title">{{test2.name}}</span>
												</div>
												<div class="card-action">
														<a href="#" @click="remove(test2.id)" >remove</a>
														<router-link :to="'test2/'+test2.id">edit</router-link>
												</div>
										</div>
								</div>
						</div>	
				</div>
				<div>
						<div class="row">
								<div class="input-field col s6">
										<input id="NameInput" v-model="newTest2.name" type="text" class="validate">
										<label class="active" for="NameInput">Name</label>
								</div>
								<div class="input-field col s6">
										<a @click="save" class="waves-effect waves-light btn">Add</a>
								</div>

						</div>
				</div>
		</div>	

`

export default {
		data(){
				return {
						test2s:[],
						newTest2:{
							name:''		
						}
				}
		},
		async created(){
				this.load()
		},
		methods:{
				async load(){
						const data = await qraphql(`{test2s{id,name}}`)
						this.test2s = data.data.test2s
						this.$forceUpdate();
				},
				async save(){
						const q = `
						mutation{
								createTest2(name: "${this.newTest2.name}"){
										id
										name
								}
						}							
						`
						const data = await qraphql(q)
						this.test2s.push(data.data.createTest2)
				},
				async remove(id){
						const q = `
						mutation{
								removeTest2(ID: "${id}")
						}
						`
						const data = await qraphql(q)
						this.load()

				}
		},
		template
}

