import Model from "./model.js"
import { makeExecutableSchema } from '@graphql-tools/schema';


export default  makeExecutableSchema({
		typeDefs: `
		type Query {
				test2s(name:String,_id:String): [Test2!]!
		}
		type Test2 {
				id: ID!
						name: String!
		}
		type Mutation {
				createTest2(name: String!): Test2!
				removeTest2(ID: ID): Boolean! 
				updateTest2(ID: ID name:String): Boolean!

		}
		`,
		resolvers:{
				Query:{
						test2s: (_,args) => Model.find(args)
				},
				Mutation:{
						createTest2:async (_, { name }) => new Model({ name }).save(),
						removeTest2:async (_, { ID}) => {
								await Model.remove({ _id:ID})
								return true 
						},
						updateTest2:async (_, { ID,name}) => {
							  await Model.updateOne({ _id:ID,name})
								return true 
						}

				}
		}
})

