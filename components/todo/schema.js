import Model from "./model.js"
import { makeExecutableSchema } from '@graphql-tools/schema';


export default  makeExecutableSchema({
		typeDefs: `
		type Query {
				todos(name:String,_id:String): [Todo!]!
		}
		type Todo {
				id: ID!
						name: String!
		}
		type Mutation {
				createTodo(name: String!): Todo!
				removeTodo(ID: ID): Boolean! 
				updateTodo(ID: ID name:String): Boolean!

		}
		`,
		resolvers:{
				Query:{
						todos: (_,args) => Model.find(args)
				},
				Mutation:{
						createTodo:async (_, { name }) => new Model({ name }).save(),
						removeTodo:async (_, { ID}) => {
								await Model.remove({ _id:ID})
								return true 
						},
						updateTodo:async (_, { ID,name}) => {
							  await Model.updateOne({ _id:ID,name})
								return true 
						}

				}
		}
})

