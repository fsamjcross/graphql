import html from '/js/html.js'
import qraphql from '/js/qraphql.js'

const template = html`
		<div>
				<div v-for="todo in todos">
						<div class="row">
								<div class="col s12 m6">
										<div class="card red darken-3">
												<div class="card-content white-text">
														<span class="card-title">{{todo.name}}</span>
												</div>
												<div class="card-action">
														<a href="#" @click="remove(todo.id)" >remove</a>
														<router-link :to="'todo/'+todo.id">edit</router-link>
												</div>
										</div>
								</div>
						</div>	
				</div>
				<div>
						<div class="row">
								<div class="input-field col s6">
										<input id="NameInput" v-model="newTodo.name" type="text" class="validate">
										<label class="active" for="NameInput">Name</label>
								</div>
								<div class="input-field col s6">
										<a @click="save" class="waves-effect waves-light btn">Add</a>
								</div>

						</div>
				</div>
		</div>	

`

export default {
		data(){
				return {
						todos:[],
						newTodo:{
							name:''		
						}
				}
		},
		async created(){
				this.load()
		},
		methods:{
				async load(){
						const data = await qraphql(`{todos{id,name}}`)
						this.todos = data.data.todos
						this.$forceUpdate();
				},
				async save(){
						const q = `
						mutation{
								createTodo(name: "${this.newTodo.name}"){
										id
										name
								}
						}							
						`
						const data = await qraphql(q)
						this._data.todos.push(data.data.createTodo)
				},
				async remove(id){
						const q = `
						mutation{
								removeTodo(ID: "${id}")
						}
						`
						const data = await qraphql(q)
						this.load()

				}
		},
		template
}

