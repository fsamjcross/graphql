import Model from "./model.js"
import { makeExecutableSchema } from '@graphql-tools/schema';


export default  makeExecutableSchema({
		typeDefs: `
		type Query {
				{{nameL}}s(name:String,_id:String): [{{nameU}}!]!
		}
		type {{nameU}} {
				id: ID!
						name: String!
		}
		type Mutation {
				create{{nameU}}(name: String!): {{nameU}}!
				remove{{nameU}}(ID: ID): Boolean! 
				update{{nameU}}(ID: ID name:String): Boolean!

		}
		`,
		resolvers:{
				Query:{
						{{nameL}}s: (_,args) => Model.find(args)
				},
				Mutation:{
						create{{nameU}}:async (_, { name }) => new Model({ name }).save(),
						remove{{nameU}}:async (_, { ID}) => {
								await Model.remove({ _id:ID})
								return true 
						},
						update{{nameU}}:async (_, { ID,name}) => {
							  await Model.updateOne({ _id:ID,name})
								return true 
						}

				}
		}
})

