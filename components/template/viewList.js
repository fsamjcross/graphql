import html from '/js/html.js'
import qraphql from '/js/qraphql.js'

const template = html`
		<div>
				<div v-for="{{nameL}} in {{nameL}}s">
						<div class="row">
								<div class="col s12 m6">
										<div class="card red darken-3">
												<div class="card-content white-text">
														<span class="card-title">{{{{nameL}}.name}}</span>
												</div>
												<div class="card-action">
														<a href="#" @click="remove({{nameL}}.id)" >remove</a>
														<router-link :to="'{{nameL}}/'+{{nameL}}.id">edit</router-link>
												</div>
										</div>
								</div>
						</div>	
				</div>
				<div>
						<div class="row">
								<div class="input-field col s6">
										<input id="NameInput" v-model="new{{nameU}}.name" type="text" class="validate">
										<label class="active" for="NameInput">Name</label>
								</div>
								<div class="input-field col s6">
										<a @click="save" class="waves-effect waves-light btn">Add</a>
								</div>

						</div>
				</div>
		</div>	

`

export default {
		data(){
				return {
						{{nameL}}s:[],
						new{{nameU}}:{
							name:''		
						}
				}
		},
		async created(){
				this.load()
		},
		methods:{
				async load(){
						const data = await qraphql(`{{{nameL}}s{id,name}}`)
						this.{{nameL}}s = data.data.{{nameL}}s
						this.$forceUpdate();
				},
				async save(){
						const q = `
						mutation{
								create{{nameU}}(name: "${this.new{{nameU}}.name}"){
										id
										name
								}
						}							
						`
						const data = await qraphql(q)
						this.{{nameL}}s.push(data.data.create{{nameU}})
				},
				async remove(id){
						const q = `
						mutation{
								remove{{nameU}}(ID: "${id}")
						}
						`
						const data = await qraphql(q)
						this.load()

				}
		},
		template
}

