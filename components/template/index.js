import model from './model.js'
import schema from './schema.js'

export  default {
		model,
		schema,
		views:[
				"view.js",
				"viewList.js"
		],
}

