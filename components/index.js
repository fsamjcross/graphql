import todo from './todo/index.js'
import test from './test/index.js'
import test2 from './test2/index.js'
import nav from './nav/index.js'

export default {  
		todo,
		test,
		test2,
		nav,
}
