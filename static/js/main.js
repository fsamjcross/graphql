import html from './html.js'
import navbar from '../components/nav/view.js'
import todos from '../components/todo/viewlist.js'
import todo from '../components/todo/view.js'
import test from '../components/test/view.js'
import tests from '../components/test/viewlist.js'
import test2 from '../components/test2/view.js'
import tests2 from '../components/test2/viewlist.js'



const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

const routes = [
		{ path: '/foo', component: Foo ,name:"foo"},
		{ path: '/bar', component: Bar, name:"bar"},

		{ path: '/todo', component: todos, name:"todos"},
		{ path: '/todo/:id', component: todo},

		{ path: '/test2', component: test2, name:"test2"},
		{ path: '/test2/:id', component: tests2},

		{ path: '/test', component: tests, name:"tests"},
		{ path: '/test/:id', component: test},
]
const template = html`
		<div id="app">
				<navbar :routes="routes"></navbar>
				<div class="container">

						<router-view></router-view>
				</div>
		</div>
`
const router = new VueRouter({
		mode: 'history',
		routes
})

const app = new Vue({
		data:{
			routes	
		},
		router,
		components:{navbar},
		template
}).$mount('#app')
