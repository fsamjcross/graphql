import html from './html.js'

export default async query =>{
		const raw =  await fetch('/graphql', {
				method: 'POST',
				headers: {
						'Content-Type': 'application/json',
						'Accept': 'application/json',
				},
				body: JSON.stringify({query})
		})
		const data = await raw.json()
		if (data.errors){
				const htmlErrors = data.errors.map( el =>`<div>${el.message}</div>`)
				M.toast({html: htmlErrors.join('')})
		}
		return data

}
