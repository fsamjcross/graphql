import { makeExecutableSchema } from "@graphql-tools/schema"
import { stitchSchemas } from '@graphql-tools/stitch';
import express from "express"
import { graphqlHTTP } from "express-graphql"
import components from "./components/index.js"
import mongoose from "mongoose";

const app = express();

app.use("/graphql",graphqlHTTP({
		schema:stitchSchemas({
				subschemas: Object.values(components).map(el => el.schema).filter(item => item),
		})
}))

Object.keys(components).forEach(key=> {
		const value = components[key]
		value.views.forEach(path=>{
						app.use(`/components/${key}/${path}`,function(req, res) {
								res.sendFile(`components/${key}/${path}`,{root:'.'});
						});
		})
})
		

app.use('/',express.static('static')); 

await mongoose.connect("mongodb://localhost:27017/test3", {
		useNewUrlParser: true
});

app.use('/*',function(req, res) {
		res.sendFile('client.html',{root:'.'});
});


app.listen(4000, () => {
		console.info("Listening on http://localhost:4000");
});

