import replace from 'gulp-replace'
import gulp from 'gulp' 
gulp.task('newComponent', function(cb){
		const nameL = process.argv[3].substring(2).toLowerCase()
		const nameU = nameL.charAt(0).toUpperCase() + nameL.slice(1);
		gulp.src(['./components/template/**/*.js'])
				.pipe(replace('{{nameL}}', nameL))
				.pipe(replace('{{nameU}}', nameU))
				.pipe(gulp.dest('components/'+nameL));
		console.log('update "static/js/main.js" to load in to vue')
				console.log('update "components/index.js" to load in to node')
		return cb();
		});
